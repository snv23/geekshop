from django.contrib import admin
from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.main_page, name='index'),
    path('contacts.html', views.contacts_page, name='contacts'),
]

