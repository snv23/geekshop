from django.shortcuts import render, get_list_or_404
from django.core.paginator import Paginator

from products.models import Product


def main_page(request):
    template_name = 'magazine/index.html'
    query = get_list_or_404(Product)
    paginator = Paginator(query, 3)
    page = request.GET.get('page')
    items_1 = paginator.get_page(page)
    items_2 = paginator.get_page(2)
    return render(request, template_name, {'result_1': items_1, 'result_2': items_2})


def contacts_page(request):
    return render(request, "magazine/contacts.html", {})