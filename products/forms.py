from django import forms
from magazine.products.models import Product

class CategoryForms(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['title', 'snippet', 'price', 'category', 'image']
        widgets = {
            'name': forms.widgets.TextInput(
                attrs={
                    'class': 'product-name-field'
                }
            ),
            'snippet': forms.widgets.TextInput(
                    attrs={
                        'class': 'product-snippet-field'
                    }
            ),
            'price': forms.widgets.TextInput(
                attrs={
                    'class': 'product-price-field'
                }
            ),
            'category': forms.widgets.TextInput(
                attrs={
                    'class': 'product-category-field'
                }
            ),
            'image': forms.widgets.TextInput(
                attrs={
                    'class': 'product-image-field'
                }
            )
        }