from django.contrib import admin
from django.urls import path

from products.views import ProductModelDetail

app_name = 'products'

urlpatterns = [
    path('<int:pk>', ProductModelDetail.as_view(), name='product'),
]
