from datetime import datetime
from django.contrib import admin
from django.template.loader import render_to_string

from .models import Product


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = [
        'picture',
        'name',
        'price',
        'category',
        'modified',
        'created',
        'is_new',
    ]

    list_filter = [
        'category',
        'created',
        'modified',
    ]

    search_fields = [
        'name',
        'snippet',
    ]

    fieldsets = (
        (
        None, {
            'fields': ('name', 'category')
        }
        ),
        (
            'Content', {
                'fields': ('image', 'snippet', 'price')
            }
        )
    )

    def picture(self, obj):
        return render_to_string(
            'products/components/picture.html',
            {'image': obj.image.url}
        )

    def is_new(self, obj):
        today = datetime.now()
        return obj.created.date() >= today.date()