from django.db import models



class Product(models.Model):
    name = models.CharField(
        max_length=100,
        unique=True)
    snippet = models.TextField(
        blank=True,
        null=True,
        max_length=280)
    price = models.DecimalField(
        max_digits=8,
        decimal_places=2,
        default=0)
    category = models.ForeignKey(
        'category.Category',
        on_delete=models.PROTECT)
    image = models.ForeignKey(
        'images.Image',
        on_delete=models.PROTECT)
    created = models.DateTimeField(
        auto_now_add=True)
    modified = models.DateTimeField(
        auto_now=True)

    @property
    def url(self):
        return self.image.value

    def __str__(self):
        return "{} ({})".format(self.name, self.category.title)