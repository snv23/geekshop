from django.shortcuts import render
from django.views.generic import DetailView
from products.models import Product


class ProductModelDetail(DetailView):
    model = Product
    template_name = 'product_item.html'
    context_object_name = 'object'