from django.shortcuts import render, get_list_or_404
from category.models import Category

from django.http import JsonResponse


def rest_category_list(request):
    query = get_list_or_404(Category)
    data = map(
            lambda itm: {
                'title': itm.title,
                'snippet': itm.snippet,
                'created': itm.created,
                'modified': itm.modified
                },
                query
            )
    return JsonResponse(
        {
            'results': list(data)
        }
    )