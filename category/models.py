from django.db import models


class Category(models.Model):
    title = models.CharField(
        max_length=100,
        unique=True)
    snippet = models.TextField(
        blank=True,
        null=True,
        max_length=280)
    created = models.DateTimeField(
        auto_now_add=True)
    modified = models.DateTimeField(
        auto_now=True)

    def __str__(self):
        return self.title
