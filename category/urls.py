from django.contrib import admin
from django.urls import path

from . import views

app_name = 'category'

urlpatterns = [
    path('<int:pk>', views.category_page, name='category'),
]
