from django.shortcuts import render, get_list_or_404
from django.core.paginator import Paginator
from products.models import Product


def category_page(request, pk):
    template_name = 'category_item.html'
    query = get_list_or_404(Product, category_id=pk)
    paginator = Paginator(query, 3)
    page = request.GET.get('page')
    items = paginator.get_page(page)
    return render(request, template_name, {'results': items})

