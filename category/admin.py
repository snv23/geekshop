from django.contrib import admin

from .models import Category

class CategoryAdmin(admin.ModelAdmin):
    list_display = [
        'title',
        'created',
        'modified',
    ]

    list_filter = [
        'created',
        'modified',
    ]

    search_fields = [
        'title'
    ]

admin.site.register(Category, CategoryAdmin)
