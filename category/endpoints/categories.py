from django.urls import path

from category.api import rest_category_list

app_name = 'rest_category'

urlpatterns = [
    path('', rest_category_list, name='rest_list'),
]