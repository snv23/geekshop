from django.db import models


class Image(models.Model):
    name = models.CharField(
        max_length=100,
        unique=True)
    value = models.ImageField(
        upload_to='images/'
    )
    created = models.DateTimeField(
        auto_now_add=True)
    modified = models.DateTimeField(
        auto_now=True)

    @property
    def url(self):
        return self.value.url

    def __str__(self):
        return self.name