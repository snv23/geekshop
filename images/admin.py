from django.contrib import admin

from .models import Image

class ImageAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'created',
        'modified',
    ]

    list_filter = [
        'created',
        'modified',
    ]

    search_fields = [
        'name'
    ]

admin.site.register(Image, ImageAdmin)
