const Category = ({title}) => (
    '<li>' +
        '<a href=#>' +
            {title} +
        '</a>' +
    '</li>'
)

const renderData = res => {
    menuHtml = res.data.results.map(Category)
        .join('')
    menu = document.getElementById('nav_left')
    menu.innerHTML += menuHtml
}
