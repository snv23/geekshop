from django.shortcuts import redirect


class AdminGroupRequired:
    redirect_url = ''

    def dispatch(self, request, *args, **kwargs):
        if request.user.has_perms(
            [
                'products.can_create_product',
                'products.can_update_product',
                'products.can_delete_product',
            ]
        ):
            return super(AdminGroupRequired, self).dispatch(request, *args, **kwargs)
        return redirect(self.redirect_url)