from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login

def login_view(request):
    if request.method == 'POST':
        user = request.POST.get("username")
        pswd = request.POST.get("password")

        success_url = reverse_lazy('main:index')

        usr = authenticate(username=user, password=pswd)

        if usr and usr.is_active:
            login(request, usr)
            return redirect(success_url)

    return render(request, 'accounts/login.html')

