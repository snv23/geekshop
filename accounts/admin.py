from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Account

@admin.register(Account)
class AccountAdmin(UserAdmin):
    fieldsets = UserAdmin.fieldsets + (
        (
            'Extra', {
                'fields': ('avatar', 'phone')
            }
        ),
    )

    list_filter = [
        'created',
        'modified',
    ]

    search_fields = [
        'username'
    ]


