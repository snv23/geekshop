from django.db import models

from django.contrib.auth.models import AbstractUser

class Account(AbstractUser):
    avatar = models.ForeignKey(
        'images.Image',
        on_delete=models.PROTECT,
        null=True
        )
    phone = models.CharField(
        max_length=12,
        blank=True,
        null=True
    )
    created = models.DateTimeField(
        auto_now_add=True)
    modified = models.DateTimeField(
        auto_now=True)

    def __str__(self):
        return self.username
