from django.contrib import admin
from django.urls import path

from adpanel.views.category import (
    category_model_create,
    category_model_list,
    category_model_detail,
    category_model_update,
    category_model_delete
)

urlpatterns = [
    path('create', category_model_create, name='category_create'),
    path('', category_model_list, name='category_list'),
    path('<int:pk>', category_model_detail, name='category_detail'),
    path('update/<int:pk>', category_model_update, name='category_update'),
    path('delete/<int:pk>', category_model_delete, name='category_delete'),
]