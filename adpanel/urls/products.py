from django.contrib import admin
from django.urls import path

from adpanel.views.products import (
    ProductModelCreate,
    ProductModelDelete,
    ProductModelDetail,
    ProductModelList,
    ProductModelUpdate,
)

urlpatterns = [
    path('create', ProductModelCreate.as_view(), name='create'),
    path('update/<int:pk>', ProductModelUpdate.as_view(), name='update'),
    path('delete/<int:pk>', ProductModelDelete.as_view(), name='delete'),
    path('<int:pk>', ProductModelDetail.as_view(), name='detail'),
    path('', ProductModelList.as_view(), name='list'),
]
