from django.apps import AppConfig


class AdpanelConfig(AppConfig):
    name = 'adpanel'
