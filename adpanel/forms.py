from django.forms import ModelForm, widgets
from category.models import Category


class CategoryModelForm(ModelForm):
    class Meta:
        model = Category
        fields = ['title', 'snippet']
        widgets = {
            'title': widgets.TextInput(
                attrs={
                    'class': 'form-field'
                }
            ),
            'snippet':
                widgets.TextInput(
                    attrs={
                        'class': 'form-field'
                    }
                )
        }
