from django.urls import reverse_lazy
from django.views.generic import (
    ListView, DeleteView, CreateView,
    UpdateView, DetailView
)
from django.contrib.auth.mixins import LoginRequiredMixin
from accounts.mixins import AdminGroupRequired
from products.models import Product


class ProductModelCreate(LoginRequiredMixin, AdminGroupRequired, CreateView):
    model = Product
    fields = ['name', 'snippet', 'price', 'category', 'image']
    template_name = 'adpanel/create.html'
    redirect_url = reverse_lazy('main:index')
    success_url = reverse_lazy('main:index')


class ProductModelUpdate(LoginRequiredMixin, AdminGroupRequired, UpdateView):
    model = Product
    fields = ['name', 'snippet', 'price', 'category', 'image']
    template_name = 'adpanel/update.html'
    redirect_url = reverse_lazy('main:index')
    success_url = reverse_lazy('main:index')


class ProductModelDelete(LoginRequiredMixin, AdminGroupRequired, DeleteView):
    model = Product
    template_name = 'adpanel/delete.html'
    redirect_url = reverse_lazy('main:index')
    success_url = reverse_lazy('main:index')


class ProductModelList(ListView):
    model = Product
    template_name = 'adpanel/list.html'
    context_object_name = 'results'


class ProductModelDetail(DetailView):
    model = Product
    template_name = 'adpanel/detail.html'
    context_object_name = 'object'