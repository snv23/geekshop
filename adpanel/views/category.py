from django.shortcuts import render, get_list_or_404, get_object_or_404
from django.urls import reverse_lazy
from django.shortcuts import redirect

from ..forms import CategoryModelForm
from category.models import Category


def category_model_create(request):
    template_name = 'adpanel/create_category.html'
    success_url = reverse_lazy('main:index')
    form = CategoryModelForm(request.POST)

    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect(success_url)

    return render(request, template_name, {'form': form})


def category_model_update(request, pk):
    template_name = 'adpanel/create_category.html'
    success_url = reverse_lazy('main:index')
    query = get_object_or_404(Category, pk=pk)
    form = CategoryModelForm(instance=query)

    if request.method == 'POST':

        form = CategoryModelForm(request.POST, instance=query)

        if form.is_valid():
            form.save()
            return redirect(success_url)

    return render(request, template_name, {'form': form})


def category_model_list(request):
    template_name = 'adpanel/list_category.html'
    query = get_list_or_404(Category)
    return render(request, template_name, {'query': query})


def category_model_detail(request, pk):
    template_name = 'adpanel/detail_category.html'
    query = get_object_or_404(Category, pk=pk)
    return render(request, template_name, {'instance': query})

def category_model_delete(request, pk):
    template_name = 'adpanel/delete.html'
    if request.method == 'POST':
        query = get_object_or_404(Category, pk=pk)
        query.delete()
        return redirect(reverse_lazy('main:index'))

    return render(request, template_name, {})